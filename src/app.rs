use crate::{component::navbar::MenuContext, page::*, theme::Theme};
use css_style::Color;
use derive_more::Display;
use std::rc::Rc;
use strum_macros::EnumIter;
use yew::prelude::*;
use yew_router::prelude::*;

#[derive(Clone, Copy, Routable, PartialEq, EnumIter, Display)]
pub enum Route {
    #[at("/")]
    Home,
    #[at("/projects")]
    Projects,
    #[at("/contribution")]
    Contribution,
    // #[at("/job-history")]
    // #[display(fmt = "Job History")]
    // JobHistory,
    #[at("/education")]
    Education,
    #[at("/skills")]
    Skills,
    #[display(fmt = "Contact Me")]
    #[at("/contact-me")]
    ContactMe,
    #[not_found]
    #[at("/404")]
    NotFound,
}

fn switch(routes: &Route) -> Html {
    match routes {
        Route::Home => html! { <HomePage /> },
        Route::Projects => html! { <ProjectsPage /> },
        Route::Contribution => html! { <ContributionPage /> },
        // Route::JobHistory => html! { <JobHistoryPage /> },
        Route::Education => html! { <EducationPage /> },
        Route::Skills => html! { <SkillsPage /> },
        Route::ContactMe => html! { <ContactPage /> },
        Route::NotFound => html! { <NotFoundPage /> },
    }
}

pub enum Msg {
    MenuContext(MenuContext),
}

pub struct App {
    // nothing yet
    theme: Rc<Theme>,
    menu_context: Rc<MenuContext>,
}

impl Component for App {
    type Message = Msg;
    type Properties = ();

    fn create(ctx: &Context<Self>) -> Self {
        let link = ctx.link().clone();
        let update_menu_ctx = Callback::from(move |ctx| {
            link.send_message(Msg::MenuContext(ctx));
        });
        App {
            theme: Rc::new(Theme::default()),
            menu_context: Rc::new(MenuContext {
                is_hidden: true,
                update: update_menu_ctx,
            }),
        }
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::MenuContext(menu_ctx) => {
                self.menu_context = Rc::new(menu_ctx);
                true
            }
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        html! {
            <ContextProvider<Theme> context={ (*self.theme).clone() }>
                <ContextProvider<MenuContext> context={ (*self.menu_context).clone() }>
                    <BrowserRouter>
                        <Switch<Route> render ={ Switch::render(switch) } />
                    </BrowserRouter>
                </ContextProvider<MenuContext>>
            </ContextProvider<Theme>>
        }
    }
}
