use crate::{app::Route, component::button::Button, layout::page_layout::PageLayout};
use yew::prelude::*;
use yew_router::prelude::*;

#[function_component(NotFoundPage)]
pub fn not_found_page() -> Html {
    let history = use_history().unwrap();
    let go_home = Callback::from(move |_| history.push(Route::Home));
    html! {
        <PageLayout>
            <h1>{ "Page not found" }</h1>
            <Button label="Go Home" onclick={go_home} />
        </PageLayout>
    }
}
