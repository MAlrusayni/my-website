use crate::{
    component::{button::Button, card::Card, icon::Icon, list_text::ListText, tag::ListTags},
    layout::page_layout::PageLayout,
    theme::get_theme,
};
use css_style::unit::{percent, sec};
use css_style::Padding;
use css_style::{calc::calc, text::TextDecorationLine};
use css_style::{
    prelude::*,
    size::Length,
    text::TextAlign,
    unit::{em, px},
    Color, Display as CssDisplay, Gap, Margin,
};
use yew::prelude::*;
use yew_layout::{Align, AlignRows, AlignSelf, Column, CrossAlign, Overflow, Row};

#[function_component(ContactPage)]
pub fn contact_page() -> Html {
    html! {
        <PageLayout title="Contact Me">
            <QuickLinks />
        </PageLayout>
    }
}

#[function_component(QuickLinks)]
pub fn quick_links() -> Html {
    html! {
        <Column width={ Length::MinContent } align={ Align::Center } align_self={ AlignSelf::Center } gap={ Gap::from(px(20))}>
            <QuickLink label="GitLab" link="https://gitlab.com/MAlrusayni/" icon="fa-brands fa-gitlab" />
            <QuickLink label="Github" link="https://github.com/MuhannadAlrusayni" icon="fa-brands fa-github"/>
            <QuickLink label="Twitter" link="https://twitter.com/MuhannadRusayni" icon="fa-brands fa-twitter" />
            <QuickLink label="Telegram" link="https://t.me/Muhannad_Alrusayni" icon="fa-brands fa-telegram" />
            <QuickLink label="Linked-in" link="https://www.linkedin.com/in/muhannad-alrusayni-software-engineer/" icon="fa-brands fa-linkedin" />
        </Column>
    }
}

#[derive(Clone, PartialEq, Properties)]
pub struct QuickLinkProps {
    link: &'static str,
    icon: &'static str,
    label: &'static str,
}

#[function_component(QuickLink)]
pub fn quick_link(props: &QuickLinkProps) -> Html {
    let theme = get_theme();
    let is_hovred = use_state(|| false);

    let on_mouse_over = {
        let is_hovred = is_hovred.clone();
        Callback::from(move |_| is_hovred.set(true))
    };
    let on_mouse_leave = {
        let is_hovred = is_hovred.clone();
        Callback::from(move |_| is_hovred.set(false))
    };

    let style = style()
        .and_font(|f| f.normal_style().bold().size(em(1.0)))
        .and_text(|t| {
            t.color(if *is_hovred { theme.text } else { theme.border })
                .and_decoration(|d| d.line(TextDecorationLine::None))
        })
        .and_transition(|t| t.ease_in_out().duration(sec(0.25)))
        .to_string();

    html! {
        <a style={ style } target="_blank" href={ props.link } onmouseover={ on_mouse_over } onmouseleave={ on_mouse_leave }>
            <Row gap={ Gap::from(em(0.6)) } cross_align={ CrossAlign::Center }>
                <Icon name={ props.icon } />
                <div>{ props.label }</div>
            </Row>
        </a>
    }
}
