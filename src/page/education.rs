use crate::{
    component::{button::Button, card::Card, list_text::ListText, tag::ListTags},
    layout::page_layout::PageLayout,
    theme::get_theme,
};
use css_style::{style, unit::px};
use yew::prelude::*;
use yew_layout::{Align, Column, CrossAlign, Length, Overflow, Row, Shadow};

#[function_component(EducationPage)]
pub fn education_page() -> Html {
    let theme = get_theme();
    let text = style().margin(px(0)).to_string();

    html! {
        <PageLayout title="Education" bg={theme.shadow_lvl1}>
            <Card title="Bachelors - 2021" subtitle="At College of Technology in Buraidah">
                <p style={text.clone()}>{"I studied Programming major and achieved the second class honors with GPA 4.62 of 5"}</p>
                <ListText title={"Projects I was honored for:"} items={vec![
                    "Graduate project: as best graduate project of the year.",
                    "IoT Project: as best project between the teams",
                ]}/>
                <ListText title={"Most studied topics:"} items={vec![
                    "Web development",
                    "Mobile development",
                    "IoT development",
                    "Development process",
                    "Team workflow",
                ]}/>
            </Card>

            <Card title="Diploma - 2018" subtitle="At Collage of Telecom and Information in Riyadh">
                <p style={text.clone()}>{"I studied Programming major and achieved the first class honors with GPA 4.94 of 5"}</p>
                <ListText title={"Projects I was honored for:"} items={vec![
                    "Graduate project, honored as best graduate project by my teachers.",
                ]}/>
                <ListText title={"Most studied topics:"} items={vec![
                    "Web development",
                    "PL/SQL",
                    "Mobile development",
                ]}/>
            </Card>

        </PageLayout>
    }
}
