pub mod contact;
pub mod contribution;
pub mod education;
pub mod home;
// pub mod job_history;
pub mod not_found;
pub mod projects;
pub mod skills;

pub use contact::ContactPage;
pub use contribution::ContributionPage;
pub use education::EducationPage;
pub use home::HomePage;
// pub use job_history::JobHistoryPage;
pub use not_found::NotFoundPage;
pub use projects::ProjectsPage;
pub use skills::SkillsPage;
