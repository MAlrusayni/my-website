use crate::{
    component::{button::Button, card::Card, list_text::ListText, tag::ListTags},
    layout::page_layout::PageLayout,
    theme::get_theme,
};
use css_style::{style, unit::px};
use yew::prelude::*;
use yew_layout::{Align, Column, CrossAlign, Length, Overflow, Row, Shadow};

#[function_component(SkillsPage)]
pub fn skills_page() -> Html {
    let theme = get_theme();
    let text = style().margin(px(0)).to_string();

    html! {
        <PageLayout title="Skills" bg={theme.shadow_lvl1}>
            <Card title="Web Development">
                <p style={text.clone()}>{"I am mainly web developer, so I focus on learning web technologies as much as I can."}</p>

                <ListText columns=2 title="Backend Frameworks & Libraries" items={vec![
                    "Laravel", "Rocket", "Warp", "Actix"
                ]} />
                <ListText columns=2 title="Frontend Frameworks & Libraries" items={vec![
                    "React", "Flutter", "Yew", "Seed",
                ]} />
                <ListText columns=2 title="Others" items={vec![
                    "Laravel Blade", "Handlebars", "Mustache", "Bootstrap", "TailwindCSS", "Bulma",
                ]} />
            </Card>

            <Card title="Mobile Development">
                <p style={text.clone()}>{"Even though that mobile development is not my focus, I have fairly good experiences to build fully working mobile applications"}</p>

                <ListText columns=2 title="Frameworks" items={vec![
                    "Flutter", "Xamarin", "Swift", "Android Studio",
                ]} />
            </Card>

            <Card title="Desktop Development">
                <ListText columns=2 title="Frameworks" items={vec![
                    "GTK+", "Qt",
                ]} />
            </Card>

            <Card title="DevOps">
                <ListText columns=2 items={vec![
                    "AWS", "Docker", "Git", "GitLab", "GitHub", "Trello", "Linux", "VirtualBox", "SSH",
                ]} />
            </Card>

            <Card title="Databases">
                <ListText columns=2 items={vec![
                    "PostgreSQL", "MongoDb", "Firebase", "MySQL/Mariadb", "SQLite", "Oracle PL/SQL"
                ]} />
                <ListText title="ORM" columns=2 items={vec![
                    "Diesel", "SeaORM", "Eloquent",
                ]} />
            </Card>

            <Card title="Common">
                <ListText columns=2 items={vec![
                    "Debugging", "Design Patterns", "Clean Code", "TDD", "DDD",
                ]} />
            </Card>

            <Card title="IoT">
                <ListText columns=2 items={vec![
                    "Arduino", "Raspberry Pi", "BeagleBone",
                ]} />
            </Card>

            <Card title="Programming Languages">
                <ListText columns=2 items={vec![
                    "Rust", "Java", "TypeScript", "Dart", "JavaScript", "Python",
                    "C", "PHP", "C++", "Lisp", "C#", "Bash", "Pascal", "Vala", "Assembly"
                ]} />
                <ListText columns=2 title="Other languages:" items={vec![
                    "HTML", "XML", "CSS", "JSON", "Yaml", "Toml", "Markdown", "Org",
                    "Handlebars", "Mustache", "LATEX"
                ]} />
            </Card>

        </PageLayout>
    }
}
