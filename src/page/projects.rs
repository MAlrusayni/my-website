use crate::{
    component::button::Button,
    component::card::Card,
    component::icon::Icon,
    component::list_text::ListText,
    component::tag::{ListTags, Tag},
    layout::page_layout::PageLayout,
    theme::get_theme,
};
use css_style::{
    calc::calc,
    prelude::*,
    size,
    text::{TextAlign, TextDecorationLine, TextJustify, TextOverflow, WordBreak, WordWrap},
    unit::{em, percent, px},
    Background, Border, Font, Gap, Margin, Padding, Size,
};
use palette::{ColorDifference, WithAlpha};
use yew::prelude::*;
use yew_layout::{Align, Column, CrossAlign, Length, Overflow, Row, Shadow};

#[function_component(ProjectsPage)]
pub fn projects_page() -> Html {
    let theme = get_theme();
    let text = style().margin(px(0)).to_string();

    html! {
        <PageLayout title="My Projects" bg={ theme.shadow_lvl1}>
            <Card title="BCT Gate" subtitle="Bachelor Graduate Project">
                <p style={ text.clone()}>{
                    "This is my graduate project. It was honored and adopted by my college.
                    It is a financial accounting system." }</p>

                <p style={ text.clone() }>{
                    "It helps the college staff to manage the studying hours for students
                    according to their available balance, and helps students to register
                    into the available subjects per-term taking into account their
                    available balance." }</p>

                <p style={ text.clone() }>{
                    "The system provide robust permissions & rules mechanism to allow
                    multiple staff rules with different privilege according to their
                    permissions." }</p>

                <p style={ text.clone() }>{
                    "It also has money transaction mechanism to log every single money
                    transaction." }</p>

                <p style={ text.clone() }>{
                    "I worked as developer in team of 6 pepole, 3 as developers and 3
                    as documentary, we worked 90% remotely using Zoom & GitHub 10%
                    face-to-face meeting" }</p>

                <ListText title={ "I worked on the following:" } items={ vec![
                    "Lead the team in the development process",
                    "Reviewed my teammates code",
                    "Analyzed and programmed the authorization system and designed its SQL schema",
                    "Analyzed and programmed the money transaction system and designed its SQL schema",
                    "Tested the websites pages",
                    "Deployed the website into AWS hosting using Docker",
                    "Assisted in the documentation process",
                    "Analyzed the project requirements with client and mapped them into task using Trello",
                    "Organized team meeting using Zoom",
                    "Assisted my team mates to work with Git and Github",
                ] }/>

                <ListTags label="Technology used:" tags={ vec!["PHP", "Laravel", "SQLite", "MySql", "HTML", "CSS", "AWS", "Docker", "Trello", "Zoom"] } />
                <Row length={ Length::Auto } align={ Align::End } cross_align={ CrossAlign::Center } gap={ Gap::from(px(6)) }>
                    <Button link="https://github.com/MuhannadAlrusayni/FinalProject" label="Link" />
                </Row>
            </Card>

            <Card title="CTI Gate" subtitle="Diploma Graduate Project">
                <p style={ text.clone() }>{
                    "This is my diploma graduate project, it's website that allows students to send their
                    ideas to college creative department with control panel for the college staff." }</p>

                <p style={ text.clone() }>{
                    "The website also offers a CMS features such as publishing posts, adding students clubs,
                            publishing online courses, things like that." }</p>

                <p style={ text.clone() }>{
                    "Students can create their accounts to sign-in for students clubs or sending their
                            creative ideas" }</p>

                <p style={ text.clone() }>{
                    "The college staff can manage all students ideas as well as the CMS features form the
                            control panel." }</p>

                <ListText title={ "Most notable tasks:" } items={ vec![
                    "Analyzed the project requirements",
                    "Developed the entire project",
                ] }/>

                <ListTags label="Technology used:" tags={ vec!["Rust", "Rocket", "SQLite", "Handlebars", "HTML", "Bootstrap", "JavaScript"] } />
                <Row length={ Length::Auto } align={ Align::End } cross_align={ CrossAlign::Center } gap={ Gap::from(px(6)) }>
                    <Button link="https://gitlab.com/MAlrusayni/CTI-Gate" label="Link" />
                </Row>
            </Card>

            <Card title="Savory" subtitle="Side Project">
                <p style={ text.clone() }>{
                    "A library for building user interface based on Seed, I started this library when I was
                    practicing frontend development using Rust and Seed & Yew."  }</p>

                <p style={ text.clone() }>{
                    "This library contains common UI components (e.g. Button, Switch, TextInput) as well as a
                    Router component" }</p>

                <ListText title={ "Most notable tasks:" } items={ vec![
                    "Studying UI design patterns",
                    "Studying Web API",
                    "Studying CSS",
                    "Studying Ant Design System",
                    "Trying to replicate Ant in Savory (sort of)",
                    "Programmed UI components",
                ] }/>

                <ListTags label="Technology used:" tags={ vec!["Rust", "Seed", "HTML", "WebAssembly"] } />
                <Row length={ Length::Auto } align={ Align::End } cross_align={ CrossAlign::Center } gap={ Gap::from(px(6)) }>
                    <Button link="https://gitlab.com/MAlrusayni/savory/issues" label="Join Me" />
                    <Button link="https://gitlab.com/MAlrusayni/savory" label="Link" />
                </Row>
            </Card>

            <Card title="Yew Layout" subtitle="Side Project">
                <p style={ text.clone() }>{ "A library that provides layout components for Yew based web apps" }</p>

                <p style={ text.clone() }>{
                    "It's core components are Column and Row, with these two components you can build very
                    complex layout with ease" }</p>

                <ListText title={ "Most notable tasks:" } items={ vec![
                    "Studying Elm Row & Column layout",
                    "Studying CSS",
                    "Creating & Testing components",
                ] }/>

                <ListTags label="Technology used:" tags={ vec!["Rust", "Yew", "HTML", "WebAssembly"] } />
                <Row length={ Length::Auto } align={ Align::End } cross_align={ CrossAlign::Center } gap={ Gap::from(px(6)) }>
                    <Button link="https://gitlab.com/MAlrusayni/yew-layout/issues" label="Join Me" />
                    <Button link="https://gitlab.com/MAlrusayni/yew-layout" label="Link" />
                </Row>
            </Card>

            <Card title="CSS Style" subtitle="Side Project">
                <p style={ text.clone() }>{ "A library that provides typed CSS style with builder-style methods." }</p>
                <ListText title={ "Most notable tasks:" } items={ vec!["Studying CSS"] }/>
                <ListTags label="Technology used:" tags={ vec!["Rust", "CSS", "HTML"] } />
                <Row length={ Length::Auto } align={ Align::End } cross_align={ CrossAlign::Center } gap={ Gap::from(px(6)) }>
                    <Button link="https://gitlab.com/MAlrusayni/css-style/issues" label="Join Me" />
                    <Button link="https://gitlab.com/MAlrusayni/css-style" label="Link" />
                </Row>
            </Card>

            <Card title="Home Keeper" subtitle="Side Project">
                <p style={ text.clone() }>{ "This little tool helps doing home-freeze like for *nix like systems." }</p>
                <ListText title={ "Most notable tasks:" } items={ vec!["Studying GNU/Linux system", "Studying rsync"] }/>
                <ListTags label="Technology used:" tags={ vec!["Rust", "rsync", "GNU/Linux", "GTK+"] } />
                <Row length={ Length::Auto } align={ Align::End } cross_align={ CrossAlign::Center } gap={ Gap::from(px(6)) }>
                    <Button link="https://gitlab.com/MAlrusayni/home-keeper" label="Link" />
                </Row>
            </Card>

            <Card title="Xyz" subtitle="Side Project">
                <p style={ text.clone() }>{
                    "Xyz is a library written in Vala language, it provide the basic
                    geometry object and their related methods to make it easy to deal with
                    these objects." }</p>
                <ListText title={ "Most notable tasks:" } items={ vec!["Studying algebraic geometry"] }/>
                <ListTags label="Technology used:" tags={ vec!["Vala"] } />
                <Row length={ Length::Auto } align={ Align::End } cross_align={ CrossAlign::Center } gap={ Gap::from(px(6)) }>
                    <Button link="https://gitlab.com/MAlrusayni/Xyz/" label="Link" />
                </Row>
            </Card>

            <Card title="Teacher Hand" subtitle="Side Project">
                <p style={ text.clone() }>{ "Teacher Hand is an application that help you make tutorial in image/PDF format." }</p>
                <ListText title={ "Most notable tasks:" } items={ vec!["Studying algebraic geometry", "Studying GTK-rs"] }/>
                <ListTags label="Technology used:" tags={ vec!["Rust", "GTK+"] } />
                <Row length={ Length::Auto } align={ Align::End } cross_align={ CrossAlign::Center } gap={ Gap::from(px(6)) }>
                    <Button link="https://gitlab.com/MAlrusayni/teacher-hand/" label="Link" />
                </Row>
            </Card>

            <Card title="News Articles App" subtitle="Internship Project">
                <p style={ text.clone() }>{ "This is a web app (SPA) client for newsapi.org" }</p>
                <ListText title={ "Most notable tasks:" } items={ vec!["Studying newsapi.org API", "Studying React Context"] }/>
                <ListTags label="Technology used:" tags={ vec!["TypeScript", "React", "CSS", "tailwindcss"] } />
                <Row length={ Length::Auto } align={ Align::End } cross_align={ CrossAlign::Center } gap={ Gap::from(px(6)) }>
                    <Button link="https://github.com/MuhannadAlrusayni/news-articles" label="Link" />
                </Row>
            </Card>

            <Card title="Movies Search Engine" subtitle="Internship Project">
                <p style={ text.clone() }>{ "This is a web app (SPA) client for themoviedb.org" }</p>
                <ListText title={ "Most notable tasks:" } items={ vec!["Studying themoviedb.org API", "Studying Ant Design System"] }/>
                <ListTags label="Technology used:" tags={ vec!["TypeScript", "React", "Ant Design System"] } />
            </Card>

        </PageLayout>
    }
}
