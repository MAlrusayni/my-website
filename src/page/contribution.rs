use crate::{
    component::{button::Button, card::Card, list_text::ListText, tag::ListTags},
    layout::page_layout::PageLayout,
    theme::get_theme,
};
use css_style::{style, unit::px};
use yew::prelude::*;
use yew_layout::{Align, Column, CrossAlign, Gap, Length, Overflow, Row};

#[function_component(ContributionPage)]
pub fn contribution_page() -> Html {
    let theme = get_theme();
    let text = style().margin(px(0)).to_string();

    html! {
        <PageLayout title="My Contributions" bg={theme.shadow_lvl1}>
            <Card title="Rust Tutorial 🦀" subtitle="Tutorial in Arabic">
                <p style={text.clone()}>{"I have started a Rust tutorial in Arabic based on The Book, and I think it's the only Rust tutorial in Arabic at the time I started it."}</p>
                <ListText title={"Reference"} items={vec![
                    "The official Rust book (The Book)",
                    "My own experiences in Rust",
                ]}/>
                <Row align={Align::End} cross_align={CrossAlign::Center} gap={Gap::from(px(6))}>
                    <Button link="https://www.youtube.com/playlist?list=PL0jkvqZWrX43OyAKikHIGSG2fNCjCCoW5" label="Watch" />
                </Row>
            </Card>

            <Card title="Arabic Rust Community" subtitle="Telegram Group">
                <p style={text.clone()}>{"I have started a Telegram group for Arabic Rust Community to help newcomers during their journey learning Rust."}</p>
                <p style={text.clone()}>{"We also share the latest news about Rust and common used libraries(crates)."}</p>
                <Row align={Align::End} cross_align={CrossAlign::Center} gap={Gap::from(px(6))}>
                    <Button link="https://t.me/rust_ar" label="Join Us" />
                </Row>
            </Card>

            <Card title="GNOME" subtitle="Open Source Project">
                <p style={text.clone()}>{"GNOME is a free and open-source desktop environment for Unix-like operating systems."}</p>
                <ListText title={"My contributions:"} items={vec![
                    "Translated a few apps from English into Arabic language using Gettext utilities",
                    "Reported a few bugs",
                ]}/>
                <Row align={Align::End} cross_align={CrossAlign::Center} gap={Gap::from(px(6))}>
                    <Button link="https://www.gnome.org/" label="Website" />
                </Row>
            </Card>

            <Card title="Yew" subtitle="Open Source Project">
                <p style={text.clone()}>{"Yew is a modern Rust framework for creating multi-threaded front-end web apps using WebAssembly."}</p>
                <ListText title={"My contributions:"} items={vec![
                    "Improve props handling",
                    "Reported a few bugs",
                    "Created a few libraries to work with Yew",
                ]}/>
                <Row align={Align::End} cross_align={CrossAlign::Center} gap={Gap::from(px(6))}>
                    <Button link="https://github.com/yewstack/yew/pull/1834" label="Github" />
                    <Button link="https://yew.rs/" label="Website" />
                </Row>
            </Card>

            <Card title="Seed" subtitle="Open Source Project">
                <p style={text.clone()}>{"Seed is a frontend Rust framework for creating fast and reliable web apps with an elm-like architecture."}</p>
                <ListText title={"My contributions:"} items={vec![
                    "Improve event handling",
                    "Improve style!{} macro",
                    "Created a few libraries to work with Seed",
                ]}/>
                <Row align={Align::End} cross_align={CrossAlign::Center} gap={Gap::from(px(6))}>
                    <Button link="https://github.com/seed-rs/seed/commits?author=MuhannadAlrusayni" label="Github" />
                    <Button link="https://seed-rs.org/" label="Website" />
                </Row>
            </Card>

            <Card title="SeaORM" subtitle="Open Source Project">
                <p style={text.clone()}>{"SeaORM is a relational ORM to help you build web services in Rust with the familiarity of dynamic languages."}</p>
                <ListText title={"My contributions:"} items={vec![
                    "Improve API around raw SQL",
                    "Simplified querying data",
                    "Improve Entities validation API",
                    "Opened a few issues",
                ]}/>
                <Row align={Align::End} cross_align={CrossAlign::Center} gap={Gap::from(px(6))}>
                    <Button link="https://github.com/SeaQL/sea-orm/commits?author=MuhannadAlrusayni" label="Github" />
                    <Button link="https://www.sea-ql.org/SeaORM/" label="Website" />
                </Row>
            </Card>

            <Card title="SeaQuery" subtitle="Open Source Project">
                <p style={text.clone()}>{"A dynamic query builder for MySQL, Postgres and SQLite"}</p>
                <ListText title={"My contributions:"} items={vec![
                    "Add returning functionality for delete statement",
                    "Simplified querying data",
                    "Improve ValueType API",
                    "Opened a few issues",
                ]}/>
                <Row align={Align::End} cross_align={CrossAlign::Center} gap={Gap::from(px(6))}>
                    <Button link="https://github.com/SeaQL/sea-query/commits?author=MuhannadAlrusayni" label="Github" />
                    <Button link="https://www.sea-ql.org/SeaORM/" label="Website" />
                </Row>
            </Card>

        </PageLayout>
    }
}
