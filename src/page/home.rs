use crate::layout::page_layout::PageLayout;
use crate::theme::get_theme;
use crate::{
    component::{icon::Icon, image::Image},
    theme::Theme,
};
use css_style::calc::calc;
use css_style::unit::{percent, sec, vh, vw};
use css_style::{
    color,
    prelude::*,
    text::TextAlign,
    unit::{em, px},
    Color, Display as CssDisplay, Gap, Margin,
};
use css_style::{Padding, Size};
use yew::prelude::*;
use yew_layout::Border;
use yew_layout::{Align, AlignRows, AlignSelf, Column, CrossAlign, Length, Overflow, Row, Spacer};

#[function_component(HomePage)]
pub fn home_page() -> Html {
    html! {
        <PageLayout>
            <Logo />
            <Content />
        </PageLayout>
    }
}

#[function_component(Logo)]
pub fn logo() -> Html {
    let theme = get_theme();
    html! {
        <Image src="logo.jpg"
               padding={Padding::from(px(4))}
               margin={Margin::from(px(12))}
               border={Border::default().dashed().width(px(2i32)).color(theme.border).radius(0.5)}
               min_height={Length::from(0.4)}
               aspect_ratio={(1,1)}/>
    }
}

#[function_component(Content)]
pub fn content() -> Html {
    let theme = get_theme();
    let title_style = style()
        .and_font(|f| f.size(em(2.0)).weight_600())
        .to_string();
    let title2_style = style()
        .and_font(|f| f.size(em(1.6)).weight_600())
        .and_margin(|m: Margin| m.zero().top(px(8)).bottom(px(4)))
        .to_string();
    let text_style = style()
        .and_text(|c| c.letter_spacing(px(1)))
        .and_font(|f| f.size(em(1.2)))
        .and_margin(|m| m.zero().bottom(px(18)))
        .to_string();
    html! {
        <Column width={Length::from(1.0)}
                length={ Length::Auto }
                padding={ Padding::default().x(px(10)) }
                align={ Align::Start }
                cross_align={ CrossAlign::Stretch }>

            <div style={title_style.clone()}>{ "Hi There o/" }</div>
            <div style={title_style.clone()}>{ "I'm Muhannad" }</div>
            <div style={title_style.clone()}>{ "A web developer" }</div>

            <Spacer min={Length::from(em(1.0))} />

            <p style={text_style.clone()}>
                { "I strive to make the web a better world for humans, I contribute to open source
                   in my free time (see the Contribution page)." }
            </p>

            <p style={text_style.clone()}>
                { "I use GNU/Linux as my main driver, I love using Rust language, I have also
                   started a Rust tutorial in Arabic, I also started Telegram group for Arabic
                   Rust community." }
            </p>

            <p style={text_style.clone()}>
                { "Feel free to ask me about anything I know, I will be glad to help." }
            </p>

            <p style={title2_style.clone()}>{ "About this site" }</p>
            <p style={text_style.clone()}>
                { "This is my personal website, here you will find information about me such as my
                   contributions, my projects ..etc, and my contact info." }
            </p>

            <p style={text_style.clone()}>
                { "I have categorized the site into serverial pages, you can reach these pages using ( " }
                <Icon color_on_hover={theme.primary} size=24 name="fa-solid fa-magnifying-glass"/>
                { " ) button on bottom corner." }
            </p>

        </Column>
    }
}
