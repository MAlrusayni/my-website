use crate::{
    component::button::Button, component::icon::Icon, layout::page_layout::PageLayout,
    theme::get_theme,
};
use css_style::{
    prelude::*,
    size,
    text::{TextAlign, TextDecorationLine, TextJustify, TextOverflow, WordBreak, WordWrap},
    unit::{em, px},
    Background, Border, Gap, Margin, Padding, Size,
};
use palette::{ColorDifference, WithAlpha};
use yew::prelude::*;
use yew_layout::{Align, Column, CrossAlign, Length, Overflow, Row, Shadow};

#[derive(Clone, PartialEq, Properties)]
pub struct Props {
    pub title: Option<&'static str>,
    pub items: Option<Vec<&'static str>>,
    pub gap: Option<i32>,
    pub columns: Option<u32>,
}

#[function_component(ListText)]
pub fn list_text(props: &Props) -> Html {
    let theme = get_theme();

    let text = style().text(theme.text);
    let title_style = text.clone().and_font(|f| f.bold()).to_string();
    let text = text.to_string();

    let items: Option<Vec<Html>> = props.items.as_ref().map(|items| {
        let columns_num = props.columns.unwrap_or(1);
        let chunk_size = (items.len() as f32 / columns_num as f32).ceil() as usize;

        items
            .chunks(chunk_size)
            .map(|chunk| {
                let items: Vec<Html> = chunk
                    .iter()
                    .enumerate()
                    .map(|(i, item)| {
                        html! {
                            <Row gap={ Gap::from(px(4)) }>
                                <div>{ "•" }</div>
                                <div key={ i } style={ text.clone() }>{ item }</div>
                            </Row>
                        }
                    })
                    .collect();
                html! {
                    <Column width={ Length::from(1.0) }
                            gap={ Gap::from(px(4)) }>
                        { for items }
                    </Column>
                }
            })
            .collect()
    });

    html! {
        <Column gap={ Gap::from(px(props.gap.unwrap_or(6))) }>
            if let Some(title) = props.title {
                <div style={ title_style }>{ title }</div>
            }
            if let Some(items) = items {
                <Row>
                    { for items }
                </Row>
            }
        </Column>
    }
}
