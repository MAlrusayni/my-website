use crate::{
    component::button::Button, component::icon::Icon, component::tag::Tag,
    layout::page_layout::PageLayout, theme::get_theme,
};
use css_style::{
    prelude::*,
    size,
    text::{TextAlign, TextDecorationLine, TextJustify, TextOverflow, WordBreak, WordWrap},
    unit::{em, px},
    Background, Border, Font, Gap, Margin, Padding, Size, Text,
};
use palette::{ColorDifference, WithAlpha};
use yew::prelude::*;
use yew_layout::{Align, Column, CrossAlign, Length, Overflow, Row, Shadow};

#[derive(Clone, PartialEq, Properties)]
pub struct CardProps {
    #[prop_or_default]
    pub children: Children,

    pub title: &'static str,
    pub subtitle: Option<&'static str>,
}

#[function_component(Card)]
pub fn card(props: &CardProps) -> Html {
    let theme = get_theme();
    let title = style()
        .and_font(|f| f.weight_600().size(em(1.5)))
        .to_string();
    let subtitle_style = style()
        .text(theme.text_dim)
        .and_font(|f| f.size(em(1.0)))
        .to_string();
    html! {
        <Column padding={ Padding::from(px(10)) }
                margin={ Margin::from(px(6)) }
                background={ Background::from(theme.background) }
                border={ Border::default().solid().color(theme.border).width(px(2)).radius(px(4)) }
                shadow={ Shadow::default().blur(px(2)).spread(px(2)).color(theme.shadow_lvl3) }
                gap={ Gap::from(em(1.0)) }
                length={ Length::Auto }>

            // header
            <Column gap={ Gap::from(em(0.5)) }>
                <div style={ title }>{ props.title }</div>
                if let Some(subtitle) = props.subtitle.clone() {
                    <div style={ subtitle_style }>{ subtitle }</div>
                }
            </Column>

            // body
            { props.children.clone() }
        </Column>
    }
}
