use crate::component::icon::Icon;
use css_style::{
    border::Radius,
    font,
    prelude::*,
    text::TextDecorationLine,
    unit::{self, em, percent, px, sec},
    Cursor, Gap,
};
use yew::prelude::*;
use yew_layout::{Align, CrossAlign, Row};
use yew_router::prelude::*;

use crate::theme::{get_theme, Theme};

#[derive(Properties, Clone, PartialEq)]
pub struct Props {
    #[prop_or_default]
    pub onclick: Option<Callback<MouseEvent>>,
    #[prop_or_default]
    pub label: Option<String>,
    #[prop_or_default]
    pub active: bool,
    #[prop_or_default]
    pub icon: Option<&'static str>,
    #[prop_or_default]
    pub link: Option<&'static str>,
    #[prop_or_default]
    pub round: bool,
    #[prop_or_default]
    pub font: Option<font::Font>,
    #[prop_or_default]
    pub borderless: bool,
}

#[function_component(Button)]
pub fn button(props: &Props) -> Html {
    let theme = get_theme();
    let is_hovred = use_state(|| false);
    let on_mouse_over = {
        let is_hovred = is_hovred.clone();
        Callback::from(move |_| is_hovred.set(true))
    };
    let on_mouse_leave = {
        let is_hovred = is_hovred.clone();
        Callback::from(move |_| is_hovred.set(false))
    };

    let style = style()
        .and_transition(|t| t.duration(sec(0.15)))
        .and_padding(|p| p.x(px(12)).y(px(8)))
        .background(match props.active {
            true => theme.primary,
            false => theme.background,
        })
        .and_border(|b| {
            let b = b.radius(if props.round {
                Radius::from(0.5)
            } else {
                px(4).into()
            });
            if !props.active & !props.borderless {
                b.solid().width(px(1)).color(match *is_hovred {
                    true => theme.primary,
                    false => theme.border,
                })
            } else {
                b.none()
            }
        })
        .and_text(|t| {
            t.color(match (*is_hovred, props.active) {
                (true, true) => theme.primary_hovered,
                (false, true) => theme.text_on_primary,
                (true, false) => theme.primary,
                (false, false) => theme.text,
            })
            .and_decoration(|d| {
                if props.link.is_some() {
                    d.line(TextDecorationLine::None)
                } else {
                    d
                }
            })
        })
        .font(
            props
                .font
                .clone()
                .unwrap_or_else(|| font::Font::default().size(em(1.0))),
        )
        .insert("box-sizing", "border-box")
        .cursor(if props.onclick.is_some() | props.link.is_some() {
            Cursor::Pointer
        } else {
            Cursor::Default
        })
        .to_string();

    html! {
        <a style={ style }
           href={ props.link }
           target={ props.link.is_some().then(|| "_blank") }
           onclick={ props.onclick.clone() }
           onmouseover={ on_mouse_over }
           onmouseleave={ on_mouse_leave }>
            <Row gap={ Gap::from(px(6)) } cross_align={ CrossAlign::Center } align={ Align::Center }>
                if let Some(icon) = props.icon {
                    <Icon name={ icon } />
                }
                if let Some(ref label) = props.label {
                    { label.clone() }
                }
            </Row>
        </a>
    }
}
