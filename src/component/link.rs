use css_style::unit::vw;
use css_style::{
    prelude::*,
    unit::{px, sec},
    Color, Margin, Padding, Position,
};
use yew::prelude::*;
use yew_layout::Border;
use yew_layout::Row;
use yew_layout::{Align, Column, Length, Overflow};

use crate::theme::get_theme;

#[derive(Clone, PartialEq, Properties)]
pub struct Props {
    #[prop_or_default]
    pub children: Children,

    pub url: &'static str,
}

#[function_component(Link)]
pub fn link(props: &Props) -> Html {
    let theme = get_theme();

    let is_hovred = use_state(|| false);
    let on_mouse_over = {
        let is_hovred = is_hovred.clone();
        Callback::from(move |_| is_hovred.set(true))
    };
    let on_mouse_leave = {
        let is_hovred = is_hovred.clone();
        Callback::from(move |_| is_hovred.set(false))
    };

    let link = style()
        .and_text(|t| {
            t.and_decoration(|t| t.line_none()).color(if *is_hovred {
                theme.primary
            } else {
                theme.text
            })
        })
        .and_transition(|t| t.ease_in_out().duration(sec(0.25)))
        .to_string();
    html! {
        <a style={ link }
           onmouseover={ on_mouse_over }
           onmouseleave={ on_mouse_leave }
           target="_blank"
           href={ props.url }>
            { props.children.clone() }</a>
    }
}
