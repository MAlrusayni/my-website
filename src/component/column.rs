use yew::prelude::*;

#[derive(Clone, PartialEq, Properties)]
pub struct Props {
    #[prop_or_default]
    pub small: bool,
    #[prop_or_default]
    pub large: bool,
    pub name: &'static str,
}

#[function_component(Column)]
pub fn column(props: &Props) -> Html {
    let size = match (props.small, props.large) {
        (_, true) => Some("is-large"),
        (true, false) => Some("is-small"),
        _ => None,
    };
    html! {
        <span class={ classes!("icon", size) }>
            <i class={ props.name }></i>
        </span>
    }
}
