use crate::{
    component::button::Button, component::icon::Icon, layout::page_layout::PageLayout,
    theme::get_theme,
};
use css_style::{
    prelude::*,
    size,
    text::{TextAlign, TextDecorationLine, TextJustify, TextOverflow, WordBreak, WordWrap},
    unit::{em, px},
    Background, Border, Gap, Margin, Padding, Size,
};
use palette::{ColorDifference, WithAlpha};
use yew::prelude::*;
use yew_layout::{Align, Column, CrossAlign, Overflow, Row, Shadow};

#[derive(Clone, PartialEq, Properties)]
pub struct TagProps {
    pub label: &'static str,
}

#[function_component(Tag)]
pub fn tag(props: &TagProps) -> Html {
    let theme = get_theme();
    let tag = style()
        .background(theme.dark_bg)
        .text(theme.text_on_dark_bg)
        .and_padding(|p| p.x(px(4)).y(px(2)))
        .and_border(|b| b.none().radius(px(4)))
        .to_string();
    html! {
        <div style={tag}>{ props.label }</div>
    }
}

#[derive(Clone, PartialEq, Properties)]
pub struct ListTagsProps {
    pub label: Option<String>,
    pub tags: Vec<&'static str>,
}

#[function_component(ListTags)]
pub fn list_tags(props: &ListTagsProps) -> Html {
    html! {
        <Row cross_align={CrossAlign::Center} gap={ Gap::from(em(0.5)) } wrap=true>
            if let Some(label) = props.label.clone() {
                <div style={style().and_font(|f| f.bold()).to_string()}>{ label }</div>
            }
            { for props.tags.iter().enumerate().map(|(i, item)| html!{ <Tag key={i} label={item.clone()} /> }) }
        </Row>
    }
}
