use css_style::border::Radius;
use css_style::unit::em;
use css_style::unit::{px, sec};
use css_style::{color, font, prelude::*, Cursor};
use css_style::{color::palette::Srgba, prelude::*, Color, Gap};
use gloo::console::info;
use strum::IntoEnumIterator;
use web_sys::HtmlElement;
use yew::prelude::*;
use yew_layout::{Align, Column, CrossAlign, Length, Overflow, Row};
use yew_router::prelude::*;

use super::button::Button;
use super::icon::Icon;
use crate::app::Route;
use crate::theme::{get_theme, Theme};

#[derive(Clone, Debug, PartialEq)]
pub struct MenuContext {
    pub is_hidden: bool,
    pub update: Callback<MenuContext>,
}

impl MenuContext {
    pub fn toggle_and_update(self) -> Self {
        let ctx = Self {
            is_hidden: !self.is_hidden,
            update: self.update,
        };
        ctx.update.emit(ctx.clone());
        ctx
    }

    pub fn show_and_update(self) -> Self {
        let ctx = Self {
            is_hidden: false,
            update: self.update,
        };
        ctx.update.emit(ctx.clone());
        ctx
    }
}

#[function_component(Navbar)]
pub fn navbar() -> Html {
    // context values
    let theme = get_theme();
    let menu_ctx = use_context::<MenuContext>().expect("no menu context found");
    let is_hidden = menu_ctx.is_hidden;
    let history = use_history().unwrap();
    let current_route: Option<Route> = history.location().route();
    let is_hovred = use_state(|| false);
    let col_ref = use_node_ref();

    // events handlers
    let trigger_menu = {
        let menu_ctx = menu_ctx.clone();
        Callback::from(move |_| {
            menu_ctx.clone().toggle_and_update();
        })
    };
    let onmouseover = {
        let is_hovred = is_hovred.clone();
        Callback::from(move |_| is_hovred.set(true))
    };
    let onmouseleave = {
        let is_hovred = is_hovred.clone();
        Callback::from(move |_| is_hovred.set(false))
    };
    let btn_style = style()
        .and_size(|s| {
            if is_hidden {
                s.all(px(58))
            } else {
                s.width(0.5)
                    .max_width(px(250))
                    .all_heights(Length::MinContent)
            }
        })
        .background(color::named::WHITE)
        .and_border(|b| {
            b.width(px(1))
                .solid()
                .color(theme.border)
                .radius(if is_hidden {
                    Radius::from(1.0)
                } else {
                    Radius::from(px(4))
                })
        })
        .and_box_shadow(|b| match (is_hidden, *is_hovred) {
            (false, _) => b.spread(px(6)).blur(px(8)).color(theme.shadow_lvl5),
            (true, false) => b.spread(px(1)).blur(px(4)).color(theme.shadow_lvl1),
            (true, true) => b.spread(px(3)).blur(px(4)).color(theme.shadow_lvl4),
        })
        .text(if *is_hovred {
            theme.primary
        } else {
            theme.text
        })
        .cursor(if is_hidden {
            Cursor::Pointer
        } else {
            Cursor::Default
        })
        .and_position(|p| p.fixed().bottom(px(20)).left(px(20)).z_index(2000))
        .and_transition(|t| t.ease().duration(sec(0.25)))
        .insert("overflow", "hidden")
        .to_string();

    let font_btn = font::Font::default().size(em(1.3)).bold();
    let menu_items = html! {
        <Column ref={ col_ref }
                gap={ Gap::from(px(4)) }
                align={ Align::Center }
                cross_align={ CrossAlign::Stretch }>
            // pages
            { for Route::iter().filter(|route| *route != Route::NotFound).map(|route| {
                let onclick = {
                    let menu_ctx = menu_ctx.clone();
                    let history = history.clone();
                    Callback::from(move |_| {
                        history.push(route);
                        menu_ctx.clone().show_and_update();
                    })
                };
                let is_current = current_route == Some(route);
                html! {
                    <Button font={font_btn.clone()}
                            borderless=true
                            active={is_current}
                            onclick={onclick}
                            label={route.to_string()} />
                }
            })}
            <div style={ style().and_size(|s| s.all_heights(px(1))).background(theme.border).to_string() }></div>
            <Button font={font_btn.clone()} borderless=true label={"Hide"} onclick={trigger_menu.clone()} />
        </Column>
    };

    html! {
        <button onclick={is_hidden.then(|| trigger_menu.clone())}
                onmouseover={onmouseover}
                onmouseleave={onmouseleave}
                style={ btn_style }>
            if is_hidden {
                <Icon color_on_hover={theme.primary} size=24 name="fa-solid fa-magnifying-glass"/>
            } else {
                { menu_items }
            }
        </button>
    }
}
