use css_style::{
    prelude::*,
    unit::{px, sec},
    Color,
};
use yew::prelude::*;

use crate::theme::get_theme;

#[derive(Clone, PartialEq, Properties)]
pub struct Props {
    #[prop_or(32)]
    pub size: i32,
    pub name: &'static str,
    pub color: Option<Color>,
    pub color_on_hover: Option<Color>,
}

#[function_component(Icon)]
pub fn icon(props: &Props) -> Html {
    let base_style = style()
        .and_font(|f| f.size(px(props.size)))
        .and_text(|t| t.try_color(props.color.or(Some(Color::CurrentColor))));
    if props.color_on_hover.is_some() {
        let is_hovred = use_state(|| false);
        let on_mouse_over = {
            let is_hovred = is_hovred.clone();
            Callback::from(move |_| is_hovred.set(true))
        };
        let on_mouse_leave = {
            let is_hovred = is_hovred.clone();
            Callback::from(move |_| is_hovred.set(false))
        };

        let style = base_style
            .and_text(|t| {
                t.try_color(
                    is_hovred
                        .then(|| props.color_on_hover.or(props.color))
                        .flatten(),
                )
            })
            .and_transition(|t| t.duration(sec(0.15)))
            .to_string();
        html! {
            <span style={ style} onmouseover={ on_mouse_over} onmouseleave={ on_mouse_leave }>
                <i class={ props.name }></i>
            </span>
        }
    } else {
        let style = base_style
            .and_text(|t| t.try_color(props.color))
            .to_string();
        html! {
            <span style={ style }>
                <i class={ props.name }></i>
            </span>
        }
    }
}
