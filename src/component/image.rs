use css_style::unit::vw;
use css_style::{
    prelude::*,
    unit::{px, sec},
    Color, Margin, Padding, Position,
};
use yew::prelude::*;
use yew_layout::Border;
use yew_layout::Row;
use yew_layout::{Align, Column, Length, Overflow};

use crate::theme::get_theme;

#[derive(Clone, PartialEq, Properties)]
pub struct Props {
    pub src: &'static str,
    pub aspect_ratio: Option<(u8, u8)>,
    pub min_height: Option<Length>,
    pub border: Option<Border>,
    pub margin: Option<Margin>,
    pub padding: Option<Padding>,
}

#[function_component(Image)]
pub fn image(props: &Props) -> Html {
    let img = style()
        .and_size(|s| s.max_width(1.0).max_height(1.0))
        .insert("box-sizing", "border-box")
        .insert("object-fit", "scale-down")
        .try_border(props.border.clone())
        .try_padding(props.padding.clone())
        .to_string();
    let container = style()
        .try_insert(
            "aspect-ratio",
            props.aspect_ratio.map(|(x, y)| format!("{x}/{y}")),
        )
        .try_margin(props.margin.clone())
        .to_string();
    html! {
        <Row min_width={props.min_height.clone()} overflow={Overflow::hidden()} align={Align::Center}>
            <div style={container}>
                <img style={img} src={props.src}/>
            </div>
        </Row>
    }
}
