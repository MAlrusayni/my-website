// Use `wee_alloc` as the global allocator.
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

pub mod app;
pub mod component;
pub mod layout;
pub mod page;
pub mod theme;

fn main() {
    yew::start_app::<app::App>();
}
