use css_style::{color, Color};
use palette::{FromColor, IntoColor, LinSrgb, LinSrgba, Packed, Shade, Srgb, Srgba};
use yew::use_context;

#[derive(Clone, Debug, PartialEq)]
pub struct Theme {
    // background colors
    pub primary: Color,
    pub primary_hovered: Color,
    pub secondary: Color,
    pub background: Color,
    pub dark_bg: Color,
    pub border: Color,

    // text colors
    pub text: Color,
    pub text_on_dark_bg: Color,
    pub text_dim: Color,
    pub text_on_primary: Color,

    // text sizes
    pub title_size: u8,
    pub text_size: u8,

    // shadow colors
    pub shadow_lvl1: Color,
    pub shadow_lvl2: Color,
    pub shadow_lvl3: Color,
    pub shadow_lvl4: Color,
    pub shadow_lvl5: Color,
}

impl Default for Theme {
    fn default() -> Self {
        let primary = Srgb::from(color::named::DODGERBLUE.into_format())
            .into_linear()
            .lighten(0.03);
        let primary_hovered = Srgb::from(color::named::SKYBLUE.into_format())
            .into_linear()
            .lighten(0.3);
        let text_on_primary = primary_hovered.lighten(0.9);
        let black_bg = Srgb::from(color::named::BLACK.into_format())
            .into_linear()
            .lighten(0.09);

        let text = Srgb::from(color::named::BLACK.into_format()).into_linear();
        let text_dim = text.clone().lighten(0.2);
        let text_on_black_bg: Srgb = color::named::WHITE.into_format().into_color();

        let shadow_lvl1 = Color::from((0., 0., 0., 0.05));
        let shadow_lvl2 = Color::from((0., 0., 0., 0.08));
        let shadow_lvl3 = Color::from((0., 0., 0., 0.1));
        let shadow_lvl4 = Color::from((0., 0., 0., 0.2));
        let shadow_lvl5 = Color::from((0., 0., 0., 0.35));

        Theme {
            primary: Srgb::from_color(primary).into(),
            primary_hovered: Srgb::from_color(primary_hovered).into(),
            secondary: color::named::AQUA.into(),
            background: color::named::WHITE.into(),
            dark_bg: Srgb::from_color(black_bg).into(),
            text_on_dark_bg: text_on_black_bg.into(),
            border: Srgba::from_color(LinSrgba::new(0., 0., 0., 0.25)).into(),
            text: Srgba::from_color(text).into(),
            text_dim: Srgba::from_color(text_dim).into(),
            text_on_primary: Srgb::from_color(text_on_primary).into(),
            title_size: 18,
            text_size: 12,
            shadow_lvl1,
            shadow_lvl2,
            shadow_lvl3,
            shadow_lvl4,
            shadow_lvl5,
        }
    }
}

pub fn get_theme() -> Theme {
    use_context().expect("no theme found on context")
}
