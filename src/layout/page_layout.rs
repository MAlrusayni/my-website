use crate::{component::link::Link, component::navbar::Navbar, theme::get_theme};
use css_style::{
    margin,
    prelude::*,
    text::TextAlign,
    unit::{em, px, vw},
    Background, Color, Margin, Padding,
};
use yew::prelude::*;
use yew_layout::{
    Align, AlignColumns, AlignRows, AlignSelf, Column, CrossAlign, Length, Overflow, Row,
};

#[derive(Clone, PartialEq, Properties)]
pub struct Props {
    #[prop_or_default]
    pub children: Children,

    #[prop_or_default]
    pub title: Option<String>,
    #[prop_or_default]
    pub subtitle: Option<String>,

    #[prop_or_default]
    pub bg: Option<Color>,
}

#[function_component(PageLayout)]
pub fn page_layout(props: &Props) -> Html {
    let theme = get_theme();
    let titlebar_style = style()
        .and_border(|b| b.and_bottom(|b| b.solid().solid().width(px(1)).color(theme.border)))
        .and_size(|s| s.width(1.0))
        .and_padding(|p| p.y(px(16)))
        .margin(px(0))
        .and_text(|t| t.align(TextAlign::Center))
        .and_box_shadow(|s| s.color(theme.shadow_lvl3).blur(px(4)).spread(px(2)))
        .background(theme.background)
        .to_string();
    let title_style = style()
        .and_font(|f| f.weight_600().size(em(1.9)))
        .to_string();
    let subtitle_style = style()
        .and_text(|t| t.color(theme.text_dim))
        .and_font(|f| f.size(em(1.0)))
        .to_string();
    let footer = style()
        .and_text(|t| t.color(theme.text_dim).align(TextAlign::Center))
        .padding(px(4));
    html! {
        <Column width={ Length::from(1.0) }
                cross_align={ CrossAlign::Center }
                overflow={ Overflow::hidden() }
                background={ Background::from(props.bg.unwrap_or(theme.background)) }>
            if let Some(title) = props.title.clone() {
                <div style={ titlebar_style }>
                    <div style={ title_style }>{ title }</div>
                    if let Some(subtitle) = props.subtitle.clone() {
                        <div style={ subtitle_style }>{ subtitle }</div>
                    }
                </div>
            }
            <Column width={ Length::from(vw(100)) } overflow={ Overflow::hidden().y_auto_scroll() } align={ Align::Center } cross_align={ CrossAlign::Center }>
                <Column width={ Length::from(1.0) } max_width={ Length::from(px(1200)) }>
                    { for props.children.iter() }
                    <Column length={ Length::MinContent } min_length={ Length::MinContent } margin={ Margin::default().top(margin::Length::Auto) } cross_align={ CrossAlign::Center } align={ Align::End } padding={ Padding::default().y(px(18)) }>
                        <div style={ footer.to_string() }>
                            { "Proudly powered by " }
                            <Link url="https://www.rust-lang.org/">{ "Rust" }</Link>
                            { " & " }
                            <Link url="https://yew.rs/">{ "Yew framework" }</Link>
                        </div>
                        <div style={ footer.and_padding(|p| p.x(px(80))).to_string() }>
                            { "This webapp is under Open source license, found more on " }
                            <Link url="https://gitlab.com/MAlrusayni/my-website">{ "GitLab" }</Link>
                        </div>
                    </Column>
                </Column>
            </Column>
            <Navbar />
        </Column>
    }
}
